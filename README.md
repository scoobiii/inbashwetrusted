# In Bash We Trusted!

# Rinha de Backend 🚀

Bem-vindo à Rinha de Backend, onde apenas os mais fortes sobrevivem! Este é um projeto incrível que reúne as melhores práticas de desenvolvimento backend em um único lugar.

## Descrição

Este projeto é uma aplicação web backend que utiliza Bash para apresentar ao mundo Apache HTTP Server, IBWTSQL e C++ para fornecer uma poderosa plataforma de backend para suas necessidades.

## Pré-requisitos

Certifique-se de ter instalado o Docker e Docker Compose em sua máquina antes de prosseguir.

- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Como executar

1. Clone este repositório em sua máquina local:

```bash
git clone https://github.com/scoobiii/inbashwetrusted.git
