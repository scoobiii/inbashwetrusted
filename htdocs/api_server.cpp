/*
 * Arquivo: api_server.cpp
 * Descrição: Implementação de um servidor HTTP para lidar com solicitações GET e POST,
 *            utilizando SQLite em memória para armazenamento de dados.
 * Autor: [Seu Nome]
 * Versão: 1.0
 * Data: [Data de Hoje]
 */

#include <iostream>
#include <sqlite3.h>
#include "cpp-httplib/httplib.h"
#include "json.h"

// Estrutura para representar uma coluna no banco de dados
struct Column {
    std::string name;
    std::string value;
};

// Função para executar uma consulta SQL e chamar uma função de retorno com os resultados
template <typename... Columns>
bool selectRow(sqlite3* db, const std::string& sql, std::function<void(Columns...)>&& callback, Columns&&... columns) {
    sqlite3_stmt* statement = nullptr;
    int result = sqlite3_prepare_v2(db, sql.c_str(), -1, &statement, nullptr);
    if (result != SQLITE_OK) {
        std::cerr << "Falha ao preparar a consulta: " << sqlite3_errmsg(db) << std::endl;
        return false;
    }
    while ((result = sqlite3_step(statement)) != SQLITE_DONE) {
        if (result == SQLITE_ROW) {
            int index = 0;
            ((columns.value = std::string(reinterpret_cast<const char*>(sqlite3_column_text(statement, index++)))), ...);
            callback(std::forward<Columns>(columns)...);
        } else {
            std::cerr << "Falha ao executar a consulta: " << sqlite3_errmsg(db) << std::endl;
            sqlite3_finalize(statement);
            return false;
        }
    }
    sqlite3_finalize(statement);
    return true;
}

// Função para abrir o banco de dados SQLite em memória
bool openDatabase(sqlite3** db) {
    int result = sqlite3_open(":memory:", db); // Abre o banco de dados apenas em memória
    return result == SQLITE_OK;
}

int main() {
    sqlite3* db;
    if (!openDatabase(&db)) {
        std::cerr << "Falha ao abrir o banco de dados." << std::endl;
        return 1;
    }

    httplib::Server server;

    // Manipula solicitações GET para obter informações do usuário
    server.Get("/user", [&](const httplib::Request& req, httplib::Response& res) {
        std::string name;
        std::string sql = "SELECT name FROM Users WHERE id = ?";
        int id = std::stoi(req.get_param_value("id"));
        if (selectRow(db, sql, [&name](const std::string& n) { name = n; }, Column{"id", std::to_string(id)})) {
            res.set_content(name, "text/plain");
        } else {
            res.set_content("Erro", "text/plain");
        }
    });

    // Manipula solicitações POST para adicionar um novo usuário
    server.Post("/user", [&](const httplib::Request& req, httplib::Response& res) {
        Json::Value root;
        Json::Reader reader;
        if (!reader.parse(req.body, root)) {
            res.set_content("JSON inválido", "text/plain");
            return;
        }
        int id = root["id"].asInt();
        std::string name = root["name"].asString();
        std::string sql = "INSERT INTO Users (id, name) VALUES (?, ?)";
        sqlite3_stmt* statement;
        if (sqlite3_prepare_v2(db, sql.c_str(), -1, &statement, nullptr) == SQLITE_OK) {
            int index = 1;
            sqlite3_bind_int(statement, index++, id);
            sqlite3_bind_text(statement, index++, name.c_str(), -1, SQLITE_STATIC);
            if (sqlite3_step(statement) != SQLITE_DONE) {
                res.set_content("Erro", "text/plain");
            } else {
                res.set_content("Inserido", "text/plain");
            }
            sqlite3_finalize(statement);
        } else {
            res.set_content("Falha ao preparar a consulta", "text/plain");
        }
    });

    // Inicia o servidor na porta 8080
    server.listen("localhost", 8080);

    // Fecha o banco de dados ao encerrar o programa
    sqlite3_close(db);
    return 0;
}
