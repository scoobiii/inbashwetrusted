#!/bin/bash
source /usr/local/apache2/htdocs/lib/shared.sh

echo "Starting Apache httpd server..."

# Modificado para criar o banco de dados em memória
echo "Creating database in memory..."
sqlite3 :memory: < /usr/local/apache2/db/init.sql
echo "Database created."

exec httpd-foreground
